import { join } from 'path';

const ENV_PREFIX = `RT_`;

type LOG_LEVEL = 'trace' | 'debug' | 'info' | 'warn' | 'error' | 'fatal';
export interface DBOpts {
  opts: any;
  query: { concurrentMax: number; };
  ssl?: SSLOptions;
  uri: string;
}
export interface SSLOptions {
  ssl: boolean;
  sslCA: string;
  sslCert: string;
  sslKey: string;
  [key: string]: boolean | string;
}

export const config = {
  api: {
    authRequired: true,
  },
  db: <DBOpts>{
    query: {
      concurrentMax: 2,
    },
    uri: 'mongodb://localhost/runnertracker',
    ssl: {
      ssl: false,
      sslCA: "",
      sslCert: "",
      sslKey: ""
    }
  },
  env: process.env.NODE_ENV || 'development',
  jwt: {
    secret: '5035374e6613bcbef5ed23be16ef175b',
  },
  log: {
    level: <LOG_LEVEL>'info',
    src: false,
  },
  server: {
    host: '0.0.0.0',
    port: 9002,
  },
  synchronizer: {
    /**
     * accessToken of user to get the Authorization header of the http call to the server
     * If the serverAddr requires authorization this will be needed
    */
    accessToken: '',
    /** Will not try to sync if not enabled */
    enabled: false,
    /** The id of the event we are syncing */
    eventId: '',
    /** The url to the sync endpoint on the server you want to sync with */
    serverAddr: '',
    /** Don't push to server if true */
    skipPush: false,
    /** After completing a sync how long to wait until starting another sync */
    timeBetweenSyncs: 15000, // 15 seconds
    /** If there is a conflict who wins, the server located at serverAddr (server) or this server (client) */
    whoWins: 'client' as 'server' | 'client',
    /** The local user id to attach synced data to */
    localUserId: '',
  },
  staticFiles: {
    app: './dist-app',
    livePortal: './dist-livePortal',
  },
  version: `${require('../../package').version}-localdev`, // tslint:disable-line
};

function ObjectGetterSetter(object: any, path: string[], value?: any) {
  const SET = (typeof value !== 'undefined');
  return path.reduce(
    (obj: any, key, i, arr) => {
      if (!(key in obj)) { return i === arr.length - 1 ? (void 0) : {}; }
      if (SET && i === arr.length - 1) {
        obj[key] = value;
      } else {
        return obj[key];
      }
    },
  config);
}

function loadEnvOverrides() {
  if (typeof (process && process.env) === 'undefined') { return; }
  let foundVars = Object.keys(process.env).filter(name => name.indexOf(ENV_PREFIX) === 0);

  for (let v of foundVars) {
    let actualName = v.substr(ENV_PREFIX.length);
    if (typeof process.env[v] === 'undefined') { continue; }

    // process.env
    // console.log(`Processing ENV var ${actualName}`);

    let pieces = actualName.split('_');
    let oldVal: any = ObjectGetterSetter(config, pieces);
    if (typeof oldVal == 'number') {
      ObjectGetterSetter(config, pieces, Number(process.env[v] || ''.trim()));
    } else if (typeof oldVal == 'boolean') {
      try {
        ObjectGetterSetter(config, pieces, Boolean(JSON.parse(process.env[v] || ''.trim().toLowerCase())));
      } catch (err) {
        console.warn(`Igoring ENV var ${v}; did not parse to boolean`);
      }
    } else {
      // Anything else we'll just treat as a string
      ObjectGetterSetter(config, pieces, process.env[v] || ''.trim());
    }
  }
}

// Also look for config/{node_env}.js[on] and config/local.js[on]
// (exception: don't let local config override test config)
const CONFIGS_TO_LOAD = config.env === 'test' ? [config.env] : [config.env, 'local'];
for (let cfgName of CONFIGS_TO_LOAD) {
  try {
    let path = join(__dirname, cfgName);
    let cfg: any = require(path); // tslint:disable-line
    if (cfg.loadOverrides) {
      cfg.loadOverrides(config);
    } else {
      for (let key of Object.keys(cfg)) {
        ObjectGetterSetter(config, key.split('.'), cfg[key]);
      }
    }
  } catch(e) {
    if (!(typeof e.message === 'string' && e.message.startsWith('Cannot find module'))) {
      console.warn(`Failed to parse config file ${cfgName}: ${e.message}`);
    }
  }
}
loadEnvOverrides();

// Try to override the version with the build version
try { config.version = require('../../BUILDINFO').VERSION || config.version; } // tslint:disable-line
catch(e) { }

export function getKey(key: string, defaultIfUnset: any = void 0) {
  return ObjectGetterSetter(config, key.split('.'));
}

export function setKey(key: string, value: string | number | boolean) {
    return ObjectGetterSetter(config, key.split('.'), value);
}

config.server.port = process.env.PORT ? Number(process.env.PORT) : config.server.port;
