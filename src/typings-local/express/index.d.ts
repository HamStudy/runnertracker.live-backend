import { LeanDocument, Types } from 'mongoose';
import SendResponse from '@/lib/SendResponse';
declare global {
  namespace Express {
    export interface Request {
      jwtSessionId?: Types.ObjectId;
      user?: import('@/models/user').User;
    }
    export interface Response {
      sendResponse(result: any, resCode?: number): void;
    }
  }
}