// import { version } from './package.json';
// export const __version__ = version;

interface AppErrorOptions {
  captureStack?: boolean;
  code?: number;
  data?: any;
  logError?: boolean;
  message?: string;
  msg?: string;
  /** alias for code */
  status?: number;
  /** alias for code */
  statusCode?: number;
}

interface LoggerLike {
  error: Function;
}

const DEFAULT_LOGGER: LoggerLike = console;

class AppError extends Error {
  static createCustom(name: string, defaults: AppErrorOptions = {}) {
    class CustomError extends AppError {
      constructor(msgOrOpts?: string | AppErrorOptions, code?: number, data?: any) {
        var opts = Object.assign({}, defaults);
        if (typeof msgOrOpts === 'string') {
          opts.message = msgOrOpts || opts.message || opts.msg;
          opts.code = code || opts.code || opts.status || opts.statusCode;
          opts.data = data || opts.data;
        } else {
          msgOrOpts = msgOrOpts || {};
          opts.message = msgOrOpts.message || msgOrOpts.msg || opts.message || opts.msg;
          opts.code = msgOrOpts.code || msgOrOpts.status || msgOrOpts.statusCode || opts.code || opts.status || opts.statusCode;
          opts.data = msgOrOpts.data || opts.data;
          if (typeof msgOrOpts.captureStack === 'boolean') { opts.captureStack = msgOrOpts.captureStack; }
          if (typeof msgOrOpts.logError === 'boolean') { opts.logError = msgOrOpts.logError; }
        }
        super(opts, CustomError);
        this.name = name;
      }
    }
    CustomError.prototype.name = name;

    return CustomError;
  }

  _logged: Array<{logger: LoggerLike}> = [];
  captureStack: boolean;
  code?: number;
  data: any;
  DEFAULT_LOGGER = DEFAULT_LOGGER;
  isAppError: boolean;
  logError: boolean;
  name = 'AppError';
  statusCode?: number;


  constructor(opts: string | AppErrorOptions, context?: Function) {
    super();
    opts = opts || {};
    if (typeof opts === 'string') { opts = {message: opts}; }
    this.message = opts.message || 'An error occurred.';
    this.data = opts.data || '';
    this.code = opts.code || opts.status || opts.statusCode || 500;
    this.statusCode = this.code;
    this.isAppError = true;
    this.logError = typeof opts.logError !== 'boolean' && true || !!opts.logError;
    this.captureStack = typeof opts.captureStack !== 'boolean' && true || !!opts.captureStack;
    if (this.captureStack) {
      Error.captureStackTrace(this, (context || AppError));
    }
  }

  log(logger: LoggerLike = this.DEFAULT_LOGGER) {
    if (typeof logger.error !== 'function' || !this.logError || this.logError || this._logged.length) {
      return this;
    }
    this._logged.push({logger});
    logger.error(this.toJSON());
    var stack = this.stack;
    if (stack) { logger.error(stack); }
    return this;
  }

  toJSON() {
    return {
      type: this.name,
      message: this.message,
      data: this.data,
      code: this.code,
    };
  }
  toResponseObject = this.toJSON;
}
AppError.prototype.name = 'AppError';


export default AppError;
