import { readdirSync, statSync } from 'fs';
import { basename, extname, join } from 'path';
import { logger } from './logger';

// re-export for convenience
// export { logger };

/**
 * Defaults for the load method.
 * @type {LoadOptions}
 */
const DEFAULT_LOAD_OPTS: LoadOptions = Object.freeze<LoadOptions>({
    exclude: Object.freeze<string[]>(['index']),
    extensions: Object.freeze<string[]>([extname(__filename)]),
    includeDirs: true,
});

/**
 * Determines if a file has been excluded
 * @param {string}   base     basename of the file
 * @param {string}   ext      file extension
 * @param {LoadOptipns} opts  exclusion options
 */
export function isExcluded(base: string, ext: string, path: string, opts: LoadOptions): boolean {
    const stat = statSync(path);
    const isDir = stat.isDirectory();
    const isFile = stat.isFile();
    if ((<any>opts.exclude).includes(base)) {
        return true;
    }
    if (isFile && (<any>opts.extensions).includes(ext)) {
        return false;
    }
    if (isDir && opts.includeDirs) {
        return false;
    }
    return true;
}

/**
 * Loads all modules from a given path.
 * @param {string}      basePath base
 * @param {LoadCallback} cb       function to call on successful module load
 * @param {LoadOptions}  opts     used to define what should be excluded
 */
export function load(basePath: string, cb: LoadCallback, opts?: LoadOptions): void {
    opts = Object.assign({}, DEFAULT_LOAD_OPTS, opts || {});
    // logger.trace({basePath: basePath, opts: opts});
    readdirSync(basePath).forEach(filename => {
        logger.info(filename);
        let base: string = basename(filename);
        if (base.endsWith('.d.ts')) { return; }
        const ext: string = extname(filename).toLowerCase();
        const path = join(basePath, filename);
        base = base.substr(0, base.length - ext.length);
        if (isExcluded(base, ext, path, <any>opts)) {
            logger.trace(`excluded: ${path}`);
            return;
        }
        try {
            cb(require(join(basePath, base)), base, path); // tslint:disable-line
        } catch(err) {
            logger.error(err);
        }
    });
}

/**
 * Defines the call signature for the load methods'
 * callback function. The callback function will be
 * called once for each successfully loaded entry.
 */
export interface LoadCallback {
    (mod: any, baseName?: string, path?: string): void;
}

/**
 * Options to modify the behavior of the load method.
 */
export interface LoadOptions {
    exclude?: Readonly<string[]>;     // list of filenames to exclude (default: ['index'])
    extensions?: Readonly<string[]>;  // list of extensions to include (default: ['.ts'])
    includeDirs?: boolean;  // include directories? (default: true)
}
