import express from 'express';

type AsyncRequestHandler = (req: express.Request, res: express.Response, next: express.NextFunction) => Promise<any>;
export function queryToString(x: import('express-serve-static-core').Query[number]): string {
  if (Array.isArray(x)) {
    return typeof x[0] === 'string' ? x[0] : '';
  } else {
    return typeof x === 'string' ? x : '';
  }
}

export function AsyncREST(fn: AsyncRequestHandler) {
  return (req: express.Request, res: express.Response, next: express.NextFunction) => {
    try {
      Promise.resolve(fn(req, res, next)).then(resp => {
        res.sendResponse(resp);
      }).catch(err => {
        console.warn('Unhandled rejection:', err);
        res.sendResponse(err);
      });
    } catch (err) {
      res.sendResponse(err);
    }
  };
}