import * as bunyan from 'bunyan';
import { config } from '../config';

const logOpts: bunyan.LoggerOptions = {
    level: config.log.level,
    name: 'runnertracker',
    src: config.log.src,
};
const children: any = {};

if (config.env === 'development') {
    const PrettyStream = require('bunyan-prettystream'); // tslint:disable-line
    const prettyStdOut = new PrettyStream({
        mode: 'dev',
    });
    prettyStdOut.pipe(process.stdout);
    logOpts.streams = [{
        level: config.log.level,
        stream: prettyStdOut,
        type: 'raw',
    }];
}

export const logger = bunyan.createLogger(logOpts);

/**
 * Obtain a logger for a particular module
 * @param {string}               moduleName module name
 * @param {bunyan.LoggerOptions} options    specific options for the logger
 */
export function getLogger(moduleName: string, options?: bunyan.LoggerOptions) {
    const child = children[moduleName] || logger.child(Object.assign({}, options, {module: moduleName}));
    children[moduleName] = child;
    return child;
}

export default logger;
