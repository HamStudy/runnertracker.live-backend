import logger from './lib/logger';
import { User } from './models/user';
import { config } from '@/config';


export async function bootstrap() {
  logger.debug('Loading user hamstudy');
  const username = 'rt';
  const user = await User.findOneAndUpdate(
    {username},
    {firstName: 'HamStudy', lastName: 'Company', username, $setOnInsert: {accessToken: generateApiToken()}},
    {new: true, upsert: true}
  ).exec();
  if (config.api.authRequired) {
    config.synchronizer.localUserId = user._id.toHexString();
  }
}

function random16Hex() { return (0x10000 | Math.random() * 0x10000).toString(16).substr(1); }
function generateApiToken() {
  let str = '';
  for (let i = 0; i < 8; i++) {
    str += random16Hex();
  }
  return str;
}
// generateApiToken();
