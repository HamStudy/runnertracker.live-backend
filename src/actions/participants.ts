import { Types } from 'mongoose';
import { LeanDocument } from 'mongoose';
import { Participant, ParticipantModel } from '../models/participant';
import { EntryModel } from '../models/entry';

type ParticipantAggregated = Partial<LeanDocument<ParticipantModel>> & Pick<LeanDocument<ParticipantModel>, '_id' | 'bibNumber' | 'dnfReason' | 'dnfStation' | 'eventId' | 'firstName' | 'lastName' | 'raceId'>;
const selectFields: (keyof LeanDocument<ParticipantModel>)[] = ['_id', 'bibNumber', 'dnfReason', 'dnfStation', 'eventId', 'firstName', 'lastName', 'raceId'];

export async function create(obj: ParticipantModel, userId?: Types.ObjectId) {
  const participant = new ParticipantModel({
    ...{
      firstName: 'Unknown',
      lastName: 'Participant',
    },
    ...obj,
  });
  if (userId) { participant.userId = userId; }
  const res = await participant.save!();
  return res;
}
export async function find(id: Types.ObjectId | string, userId?: Types.ObjectId) {
  if (typeof id === 'string') { id = Types.ObjectId(id); }
  const res: ParticipantAggregated[] = await ParticipantModel.aggregate([ { $match: { _id: id, expiresAt: null } }, ...getPipeline(userId), { $project: {'userId': 0} } ]).exec();
  return res && res[0];
}
export async function findByNumber(eventId: Types.ObjectId | string, bibNumber: number, userId?: Types.ObjectId) {
  if (typeof eventId === 'string') { eventId = Types.ObjectId(eventId); }
  const res: ParticipantAggregated[] = await ParticipantModel.aggregate([ { $match: { eventId, bibNumber, expiresAt: null } }, ...getPipeline(userId), { $project: {'userId': 0} } ]).exec();
  return res && res[0];
}
export async function getAll(eventId: Types.ObjectId | string, userId?: Types.ObjectId) {
  if (typeof eventId === 'string') { eventId = Types.ObjectId(eventId); }
  const res = ParticipantModel.aggregate([ { $match: { eventId, expiresAt: null } }, ...getPipeline(userId), { $project: {'userId': 0} } ]);
  return res.exec() as ParticipantAggregated[];
}
export async function update(id: string, obj: ParticipantModel, userId?: Types.ObjectId) {
  return ParticipantModel.findOneAndUpdate({_id: id, userId, expiresAt: null}, {$set: obj}).exec();
}
export async function remove(id: string, userId?: Types.ObjectId) {
  const oneWeekFromNow = new Date(Date.now() + (7 * 24 * 60 * 60 * 1000));
  await EntryModel.updateMany({participantId: id, userId, expiresAt: null}, {
    $set: { expiresAt: oneWeekFromNow },
  }).exec();
  const resp = await ParticipantModel.updateOne({_id: id, userId, expiresAt: null}, {
    $set: { expiresAt: oneWeekFromNow },
  }).exec();
  return resp.nModified === 1;
}
// export async function removeByNumber(eventId: string, bibNumber: number) {
//   const res = await ParticipantModel.deleteOne({
//     eventId,
//     bibNumber,
//   }).exec();
//   return res.deletedCount === 1;
// }

export function getPipeline(userId?: Types.ObjectId) {
  return [
    {
      $addFields: {
        all: "$$ROOT",
        restricted: selectFields.reduce((x, y) => {x[y] = `$${y}`; return x}, {} as any),
        userId: {$ifNull: ['$userId', null]},
      }
    },
    {
      $replaceRoot: {
        newRoot: {
          $cond: {
            if: {$eq: ['$userId', userId]},
            then: '$all',
            else: '$restricted',
          }
        }
      }
    },
    {
      $addFields: {
        id: '$_id',
      }
    },
    {
      $project: {
        'userId': 0,
      }
    }
  ];
}
