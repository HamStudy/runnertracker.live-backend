import Axios, { AxiosInstance } from 'axios';
import { Types } from 'mongoose';

import logger from '@/lib/logger';
import { AllData, getAllData, updateAllData } from './events';
import { EventModel } from '@/models/event';


/**
 * Returns a promise that resolves after ms amount of time
 * @param ms Time in ms to sleep before resolving
 */
function sleep(ms: number) {
  let resolve: (value?: void | PromiseLike<void>) => void;
  let promise = new Promise<void>(r => resolve = r);
  return {timeout: setTimeout(() => resolve(), ms), promise};
}

export default class Syncronizer {
  #keepSyncing: boolean = true;
  #curTimeout: NodeJS.Timeout;
  
  lastPull?: Date;
  lastPush?: Date;

  localUserId?: Types.ObjectId;
  skipPush = false;

  private axios: AxiosInstance;

  constructor(
    /** The url to the sync endpoint on the server you want to sync with */
    private serverAddr: string,
    /** The id of the event we are syncing */
    private eventId: string,
    /** After completing a sync how long to wait until starting another sync */
    private interval: number,
    /** If there is a conflict who wins, the server located at serverAddr (server) or this server (client) */
    private wins: 'client' | 'server',
    {
      accessToken,
      skipPush,
      localUserId,
    }: {
    /**
     * Token sent on the Authorization header of the http call to the server
     * If the serverAddr requires authorization this will be needed
     */
      accessToken?: string;
      skipPush?: boolean;
      localUserId?: string;
    } = {}
  ) {
    this.localUserId = new Types.ObjectId(localUserId);
    this.skipPush = !!skipPush;
    // Authenticate to server
    (async () => {
      try {
        const resp = await Axios.post(`${serverAddr}/api/user/login`, {accessToken});
        logger.info('Authenticated', resp.data);
        this.axios = Axios.create({
          headers: {
            authentication: resp.headers.authentication,
          }
        });
      } catch (e) {
        console.error(e?.response);
      }
    })();

    // Set the local userId
    (async () => {
      try {
        const event = await EventModel.findOne({_id: eventId}).lean().exec();
        if (event?.userId) {
          this.localUserId = event?.userId;
        }
      } catch (e) {
        console.error(e);
      }
    })();
  }

  async pullDataSince() {
    logger.info(`Pulling from server`);
    // Get the data from the server
    // update this database with the data received

    let url = `${this.serverAddr}/api/v1/events/${this.eventId}/allData`;
    if (this.lastPull) { url += `?sinceDate=${this.lastPull.toISOString()}`; }
    logger.info(url);
    let resp = await this.axios.get<AllData>(url);
    const { updated, inserted } = await updateAllData(resp.data, this.localUserId);
    logger.info(`Updated: ${updated}, Inserted: ${inserted}`);
    this.lastPull = new Date();
  }
  async pushDataSince() {
    logger.info(`Pushing to server`);
    // Get the data from this database
    // push it up to the server
    const allData = await getAllData(this.eventId, this.lastPush);
    const numToPush = Object.keys(allData).reduce((n, k: keyof typeof allData) => n + (allData[k].length), 0);
    if (!this.skipPush) {
      logger.info(`Pushing ${numToPush} documents`);
      const {updated, inserted} = (await this.axios.post(`${this.serverAddr}/api/v1/events/${this.eventId}/allData`, allData)).data;
      logger.info(`Updated: ${updated}, Inserted: ${inserted}`);
      this.lastPush = new Date();
    } else {
      logger.info(`Skip pushing ${numToPush} documents`);
    }
  }

  async sync() {
    if (!this.axios) { logger.info('Skipping sync, not authenticated'); return; }
    console.log();
    logger.info('Starting sync');
    try {
      if (this.wins === 'server') {
        await this.pullDataSince();
        await this.pushDataSince();
      } else {
        await this.pushDataSince();
        await this.pullDataSince();
      }
      logger.info('Sync finished');
    } catch (e) {
      logger.info('Sync Failed', e?.message);
    }
    console.log();
  }

  /**
   * Start continuously syncing to specified server
   */
  runContinuously() {
    if (!this.eventId) { return; }
    const promise = new Promise<void>(async (resolve, reject) => {
      while(this.#keepSyncing) {
        try {
          await this.sync();
        } catch (e) {
          console.error(e);
          // Depending on the error we may want to reject? or at least stop syncing
        } finally {
          let {promise: sleepPromise, timeout} = sleep(this.interval);
          this.#curTimeout = timeout;
          logger.info(`Sleeping for ${Math.ceil(this.interval / 1000)} seconds before next synchronization...`);
          await sleepPromise;
        }
      }
      resolve();
    });
    return promise;
  }

  stopSyncing() {
    this.#keepSyncing = false;
    clearTimeout(this.#curTimeout);
  }
}
