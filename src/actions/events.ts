import { UnorderedBulkOperation } from 'mongodb';
import { FilterQuery, LeanDocument, Types } from 'mongoose';
import { EventModel } from '../models/event';
import { EntryModel } from '../models/entry';
import { ParticipantModel } from '../models/participant';
import { RaceModel } from '../models/race';
import { StationModel } from '../models/station';
import * as ParticipantActions from './participants';

export async function create(obj: EventModel, userId?: Types.ObjectId) {
  const raceEvent = new EventModel(obj);
  if (userId) { raceEvent.userId = userId; }
  const res = await raceEvent.save!();
  return res;
}
export async function find(slugOrId: string) {
  const query = Types.ObjectId.isValid(slugOrId) && Types.ObjectId(slugOrId).toHexString() === slugOrId ? {_id: slugOrId, expiresAt: null} : {slug: slugOrId, expiresAt: null};
  return EventModel.findOne(query).collation({ locale: 'en', strength: 1 }).select('-userId').exec();
}
export async function getAll(userId?: Types.ObjectId) {
  const query: FilterQuery<EventModel> = {
    expiresAt: null,
  }
  if (arguments.length > 0) { query.userId = userId; }
  return EventModel.find(query).select('-userId').exec();
}
export async function update(id: string, obj: EventModel, userId?: Types.ObjectId) {
  return EventModel.findOneAndUpdate({_id: id, userId, expiresAt: null}, {$set: obj}).exec();
}
export async function remove(id: string, userId?: Types.ObjectId) {
  const oneWeekFromNow = new Date(Date.now() + (7 * 24 * 60 * 60 * 1000));
  await EntryModel.updateMany({eventId: id, userId, expiresAt: null}, {
    $set: { stationId: null, expiresAt: oneWeekFromNow },
  }).exec();
  await ParticipantModel.updateMany({eventId: id, userId, expiresAt: null}, {
    $set: { expiresAt: oneWeekFromNow },
  }).exec();
  await RaceModel.updateMany({eventId: id, userId, expiresAt: null}, {
    $set: { expiresAt: oneWeekFromNow },
  }).exec();
  await StationModel.updateMany({eventId: id, userId, expiresAt: null}, {
    $set: { expiresAt: oneWeekFromNow },
  }).exec();
  const resp = await EventModel.updateOne({_id: id, userId, expiresAt: null}, {
    $set: { expiresAt: oneWeekFromNow },
  }).exec();
  return resp.nModified === 1;
}

export async function getAllData(eventId: string | string[], sinceDate = new Date(0), userId?: Types.ObjectId): Promise<AllData> {
  const eventIds = typeof eventId === 'string' ? [eventId] : eventId;
  const requireUser = arguments.length === 3;

  const [events, entries, participants, races, stations] = await Promise.all([
    EventModel.find({_id: { $in: eventIds }, updatedAt: { $gt: sinceDate }}).select('-userId').lean().exec(),
    EntryModel.find({eventId: { $in: eventIds }, updatedAt: { $gt: sinceDate }}).select('-userId').lean().exec(),
    requireUser
      ? ParticipantModel.aggregate([ { $match: {eventId: { $in: eventIds.map(e => Types.ObjectId(e)) }, updatedAt: { $gt: sinceDate }} }, ...ParticipantActions.getPipeline(userId) ]).exec()
      : ParticipantModel.find({eventId: { $in: eventIds }, updatedAt: { $gt: sinceDate }}).select('-userId').lean().exec(),
    RaceModel.find({eventId: { $in: eventIds }, updatedAt: { $gt: sinceDate }}).select('-userId').lean().exec(),
    StationModel.find({eventId: { $in: eventIds }, updatedAt: { $gt: sinceDate }}).select('-userId').lean().exec(),
  ]);
  return { entries, events, participants, races, stations };
}

export async function updateAllData({events, entries, participants, races, stations}: AllData, userId?: Types.ObjectId) {
  const eventUpdateOp = EventModel.collection.initializeUnorderedBulkOp();
  for (let event of events) {
    let doc = new EventModel(event);
    if (userId) { doc.userId = userId; }
    eventUpdateOp.find({_id: doc._id, $or: [{userId}, {userId: null}]}).upsert().updateOne({
      $set: doc,
    });
  }
  const entryUpdateOp = EntryModel.collection.initializeUnorderedBulkOp();
  for (let entry of entries) {
    let doc = new EntryModel(entry);
    if (userId) { doc.userId = userId; }
    entryUpdateOp.find({_id: doc._id, $or: [{userId}, {userId: null}]}).upsert().updateOne({
      $set: doc,
    });
  }
  const participantUpdateOp = ParticipantModel.collection.initializeUnorderedBulkOp();
  for (let participant of participants) {
    let doc = new ParticipantModel(participant);
    if (userId) { doc.userId = userId; }
    participantUpdateOp.find({_id: doc._id, $or: [{userId}, {userId: null}]}).upsert().updateOne({
      $set: doc,
    });
  }
  const raceUpdateOp = RaceModel.collection.initializeUnorderedBulkOp();
  for (let race of races) {
    let doc = new RaceModel(race);
    if (userId) { doc.userId = userId; }
    raceUpdateOp.find({_id: doc._id, $or: [{userId}, {userId: null}]}).upsert().updateOne({
      $set: doc,
    });
  }
  const stationUpdateOp = StationModel.collection.initializeUnorderedBulkOp();
  for (let station of stations) {
    let doc = new StationModel(station);
    if (userId) { doc.userId = userId; }
    stationUpdateOp.find({_id: doc._id, $or: [{userId}, {userId: null}]}).upsert().updateOne({
      $set: doc,
    });
  }

  const res = await Promise.all([
    hasOperations(eventUpdateOp) ? eventUpdateOp.execute() : null,
    hasOperations(entryUpdateOp) ? entryUpdateOp.execute() : null,
    hasOperations(participantUpdateOp) ? participantUpdateOp.execute() : null,
    hasOperations(raceUpdateOp) ? raceUpdateOp.execute() : null,
    hasOperations(stationUpdateOp) ? stationUpdateOp.execute() : null,
  ]);

  const result = {
    inserted: res.reduce((n, r) => n + (r?.nUpserted??0), 0),
    updated: res.reduce((n, r) => n + (r?.nModified??0), 0),
    insertedIds: res.reduce((a, r) => {
      a.push(...(r?.getUpsertedIds().map(e => e._id) ?? []));
      return a;
    }, [] as Types.ObjectId[]),
  };
  return result;
}
function hasOperations(bulkOp: UnorderedBulkOperation) {
  return bulkOp.length > 0;
}


export interface AllData {
  events: LeanDocument<EventModel>[];
  entries: LeanDocument<EntryModel>[];
  participants: LeanDocument<ParticipantModel>[];
  races: LeanDocument<RaceModel>[];
  stations: LeanDocument<StationModel>[];
}
