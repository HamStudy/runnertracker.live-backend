import { FilterQuery, Types } from 'mongoose';
import { StationModel } from '../models/station';
import { EntryModel } from '../models/entry';
import { find as findRace } from '@/actions/races';

export async function create(obj: StationModel, userId?: Types.ObjectId) {
  const station = new StationModel(obj);
  if (userId) { station.userId = userId; }
  const res = await station.save!();
  return res;
}
export async function find(id: string | Types.ObjectId, eventId?: string) {
  const query: FilterQuery<StationModel> = {_id: id, expiresAt: null};
  if (eventId) { query.eventId = eventId; }
  return StationModel.findOne(query).select('-userId').exec();
}
export async function findByNumber(eventId: string, raceId: string | undefined, stationNumber: number) {
  return StationModel.findOne({
    eventId,
    raceId,
    stationNumber,
    expiresAt: null
  }).select('-userId').exec();
}
export async function getAll(eventId: string, raceId?: string | undefined) {
  if (arguments.length > 1) {
    const race = await findRace(eventId, raceId!);
    return race?.stationsPopulated;
  } else {
    return StationModel.find({eventId, expiresAt: null}).select('-userId').exec();
  }
}
export async function update(id: string, obj: StationModel, userId?: Types.ObjectId) {
  return StationModel.findOneAndUpdate({_id: id, userId, expiresAt: null}, {$set: obj}).exec();
}
export async function remove(id: string, userId?: Types.ObjectId) {
  const oneWeekFromNow = new Date(Date.now() + (7 * 24 * 60 * 60 * 1000));
  await EntryModel.updateMany({stationId: id, userId, expiresAt: null}, {
    $set: { expiresAt: oneWeekFromNow },
  }).exec();
  const resp = await StationModel.updateOne({_id: id, userId, expiresAt: null}, {
    $set: { expiresAt: oneWeekFromNow },
  }).exec();
  return resp.nModified === 1;
}
// export async function removeByNumber(eventId: string, stationNumber: number) {
//   const res = await StationModel.deleteOne({
//     eventId,
//     stationNumber,
//   }).exec();
//   return res.deletedCount === 1;
// }
