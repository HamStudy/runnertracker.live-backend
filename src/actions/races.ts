import { Types } from 'mongoose';
import { ParticipantModel } from '../models/participant';
import { RaceModel } from '../models/race';
import { StationModel } from '../models/station';

export async function create(obj: RaceModel, userId?: Types.ObjectId) {
  const race = new RaceModel(obj);
  if (userId) { race.userId = userId; }
  const res = await race.save!();
  return res;
}
export async function find(eventId: string, nameOrId: string) {
  const query = Types.ObjectId.isValid(nameOrId) && Types.ObjectId(nameOrId).toHexString() === nameOrId ? {eventId: Types.ObjectId(eventId), _id: Types.ObjectId(nameOrId), expiresAt: null} : {eventId: Types.ObjectId(eventId), name: nameOrId, expiresAt: null};
  const races = await RaceModel.aggregate([ { "$match" : query }, ...getPipeline() ]).exec();
  return races?.[0];
}
export async function getAll(eventId: string) {
  const races = await RaceModel.aggregate([ { "$match" : { "eventId" : Types.ObjectId(eventId), expiresAt: null, } }, ...getPipeline() ]).exec();
  return races;
}
export async function update(id: string, obj: RaceModel, userId?: Types.ObjectId) {
  return RaceModel.findOneAndUpdate({_id: id, userId, expiresAt: null}, {$set: obj}).exec();
}
export async function remove(id: string, userId?: Types.ObjectId) {
  const oneWeekFromNow = new Date(Date.now() + (7 * 24 * 60 * 60 * 1000));
  await ParticipantModel.updateMany({raceId: Types.ObjectId(id), userId, expiresAt: null}, {
    $set: { expiresAt: oneWeekFromNow },
  }).exec();
  await StationModel.updateMany({raceId: Types.ObjectId(id), userId, expiresAt: null}, {
    $set: { expiresAt: oneWeekFromNow },
  }).exec();
  const resp = await RaceModel.updateOne({_id: id, userId, expiresAt: null}, {
    $set: { expiresAt: oneWeekFromNow },
  }).exec();
  return resp.nModified === 1;
}

export function getPipeline() {
  return [
    { 
      "$sort": {"createdAt": 1}
    }, { 
      "$lookup" : { 
        "from" : "stations", 
        "localField" : "stations.id", 
        "foreignField" : "_id", 
        "as" : "stationsCollection"
      }
    }, { 
      "$addFields" : { 
        "stationsPopulated" : { 
          "$map" : { 
            "input" : "$stations", 
            "as" : "s", 
            "in" : { 
              "$mergeObjects" : [{ 
                "$first" : { 
                  "$filter" : { 
                    "input" : "$stationsCollection", 
                    "cond" : { "$eq" : ["$$this._id", "$$s.id"] }
                  }
                }
              }, "$$s"]
            }
          }
        }
      }
    }, {
      $addFields: {
        id: '$_id',
      }
    }, {
      $project: {
        'userId': 0,
        'stationsCollection': 0,
      }
    }
  ];
}
