import { Types } from 'mongoose';
import * as Errors from '@/lib/errors';
import { EntryModel } from '../models/entry';
import { find as findParticipant } from './participants';
import { find as findStation } from './stations';

export async function create(obj: EntryModel, userId?: Types.ObjectId) {
  const participantId = obj.participantId;
  const stationId = obj.stationId;
  if (!participantId || !stationId) { throw new Errors.InvalidRequestError(); }
  const participant = await findParticipant(participantId);
  const station = await findStation(stationId);
  if (!participant || !station) { throw new Errors.InvalidRequestError(); }
  const entry = new EntryModel(obj);
  if (userId) { entry.userId = userId; }
  const res = await entry.save!();
  return res;
}
export async function find(id: string): Promise<EntryModel>;
export async function find(participantId: string, stationId: string): Promise<EntryModel>;
export async function find(a: string, b?: string) {
  try {
    if (!b) {
      return await EntryModel.findOne({_id: a, expiresAt: null}).select('-userId').exec();
    } else {
      return await EntryModel.findOne({participantId: a, stationId: b, expiresAt: null}).select('-userId').exec();
    }
  } catch (e) {
    console.error(e);
    return null;
  }
}
export async function getAll({participantId, stationId, eventId, sinceDate}: Partial<{participantId: string; stationId: string; eventId: string; sinceDate: Date}>) {
  if (participantId) {
    return EntryModel.find({participantId, expiresAt: null}).select('-userId').exec();
  } else if (stationId) {
    const query: any = {stationId, expiresAt: null};
    if (sinceDate) {
      query.modified = {$gt: new Date(sinceDate), expiresAt: null};
    }
    return EntryModel.find(query).select('-userId').exec();
  } else if (eventId) {
    return EntryModel.find({eventId, expiresAt: null}).select('-userId').exec();
  }
  return [];
}
export async function update(id: string, obj: EntryModel, userId?: Types.ObjectId) {
  return await EntryModel.findOneAndUpdate({_id: id, userId, expiresAt: null}, {$set: obj}).exec();
}
export async function remove(id: string, userId?: Types.ObjectId) {
  const oneWeekFromNow = new Date(Date.now() + (7 * 24 * 60 * 60 * 1000));
  const resp = await EntryModel.updateOne({_id: id, userId, expiresAt: null}, {
    $set: { expiresAt: oneWeekFromNow },
  }).exec();
  return resp.nModified === 1;
}