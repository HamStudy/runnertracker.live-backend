import { express, logger} from '..';
import { config } from '../../config';
import * as Errors from '@/lib/errors';
import { AsyncREST } from '@/lib/express.utils';

export const router: express.Router = express.Router();
router.get('/', AsyncREST(status));
router.get('/liveness', AsyncREST(liveness));
router.get('/readiness', AsyncREST(readiness));

async function status(req: express.Request, res: express.Response, next: express.NextFunction) {
  return {v: config.version};
}

async function liveness(req: express.Request, res: express.Response, next: express.NextFunction) {
  return {};
}

async function readiness(req: express.Request, res: express.Response, next: express.NextFunction) {
  return {};
}
