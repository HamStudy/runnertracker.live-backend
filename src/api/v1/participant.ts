import { express, logger} from '@/api';
import * as Errors from '@/lib/errors';
import { AsyncREST } from '@/lib/express.utils';
import { requireAuth } from '@/middleware/authorization';
import * as participantActions from '@/actions/participants';
import * as entryActions from '@/actions/entry';

export const router: express.Router = express.Router();

router.post('/', requireAuth, AsyncREST(addParticipant));
router.get('/:participantId', AsyncREST(getParticipant));
router.get('/:participantId/entries', AsyncREST(getEntries));
router.put('/:participantId', requireAuth, AsyncREST(updateParticipant));
router.delete('/:participantId', requireAuth, AsyncREST(deleteParticipant));

async function addParticipant(req: express.Request, res: express.Response) {
  return await participantActions.create(req.body, req.user?._id);
}

async function getParticipant(req: express.Request, res: express.Response) {
  const result = await participantActions.find(req.params.participantId, req.user?._id)
  return result;
}

async function updateParticipant(req: express.Request, res: express.Response) {
  return await participantActions.update(req.body.id, req.body, req.user?._id);
}

async function deleteParticipant(req: express.Request, res: express.Response) {
  const result = await participantActions.remove(req.params.participantId, req.user?._id)
  return result;
}

async function getEntries(req: express.Request, res: express.Response) {
  return await entryActions.getAll({participantId: req.params.participantId});
}
