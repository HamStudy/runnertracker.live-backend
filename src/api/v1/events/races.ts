import { express, logger} from '@/api';
import * as Errors from '@/lib/errors';
import { AsyncREST } from '@/lib/express.utils';
import { requireAuth } from '@/middleware/authorization';
import * as raceActions from '@/actions/races';

export const router: express.Router = express.Router({mergeParams: true});

router.post('/', requireAuth, AsyncREST(addRace));
router.get('/', AsyncREST(getRaces));
router.get('/:nameOrRaceId', AsyncREST(getRace));
router.put('/:raceId', requireAuth, AsyncREST(updateRace));
router.delete('/:raceId', requireAuth, AsyncREST(deleteRace));

async function addRace(req: express.Request, res: express.Response) {
  return await raceActions.create(req.body, req.user?._id);
}

async function getRaces(req: express.Request, res: express.Response) {
  return await raceActions.getAll(req.params.eventId);
}

async function getRace(req: express.Request, res: express.Response) {
  const result = await raceActions.find(req.params.eventId, req.params.nameOrRaceId)
  return result;
}

async function updateRace(req: express.Request, res: express.Response) {
  return await raceActions.update(req.body.id, req.body, req.user?._id);
}

async function deleteRace(req: express.Request, res: express.Response) {
  const result = await raceActions.remove(req.params.raceId, req.user?._id);
  return result;
}

