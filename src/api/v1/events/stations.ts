import { express, logger} from '@/api';
import * as Errors from '@/lib/errors';
import { AsyncREST, queryToString } from '@/lib/express.utils';
import { requireAuth } from '@/middleware/authorization';
import * as stationActions from '@/actions/stations';

export const router: express.Router = express.Router({mergeParams: true});

router.post('/', requireAuth, AsyncREST(addStation));
router.get('/', AsyncREST(getStations));
router.get('/:stationNumber', AsyncREST(getStation));
router.put('/:stationNumber', requireAuth, AsyncREST(updateStation));

async function addStation(req: express.Request, res: express.Response) {
  return await stationActions.create(req.body, req.user?._id);
}

async function getStations(req: express.Request, res: express.Response) {
  if ('raceId' in req.query) {
    return await stationActions.getAll(req.params.eventId, queryToString(req.query.raceId) || (void 0));
  } else {
    return await stationActions.getAll(req.params.eventId);
  }
}

async function getStation(req: express.Request, res: express.Response) {
  const stationNumber = +req.params.stationNumber;
  const raceId = queryToString(req.query.raceId) || (void 0);
  if (isNaN(stationNumber)) {
    return new Errors.InvalidRequestError('Station number must be a number');
  }
  const result = await stationActions.findByNumber(req.params.eventId, raceId, stationNumber)
  return result;
}

async function updateStation(req: express.Request, res: express.Response) {
  return await stationActions.update(req.body.id, req.body, req.user?._id);
}
