import { express, logger} from '@/api';
import * as Errors from '@/lib/errors';
import { AsyncREST } from '@/lib/express.utils';
import { requireAuth } from '@/middleware/authorization';
import * as participantActions from '@/actions/participants';

export const router: express.Router = express.Router({mergeParams: true});

router.post('/', requireAuth, AsyncREST(addParticipant));
router.get('/', AsyncREST(getParticipants));
router.get('/:bibNumber', AsyncREST(getParticipant));
router.put('/:bibNumber', requireAuth, AsyncREST(updateParticipant));

async function addParticipant(req: express.Request, res: express.Response) {
  return await participantActions.create(req.body, req.user?._id);
}

async function getParticipant(req: express.Request, res: express.Response) {
  const result = await participantActions.findByNumber(req.params.eventId, Number(req.params.bibNumber), req.user?._id)
  return result;
}

async function getParticipants(req: express.Request, res: express.Response) {
  return await participantActions.getAll(req.params.eventId, req.user?._id);
}

async function updateParticipant(req: express.Request, res: express.Response) {
  return await participantActions.update(req.body.id, req.body, req.user?._id);
}
