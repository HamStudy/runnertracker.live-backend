import { express, logger, register} from '@/api';
import * as Errors from '@/lib/errors';
import { AsyncREST, queryToString } from '@/lib/express.utils';
import { requireAuth } from '@/middleware/authorization';
import * as eventActions from '@/actions/events';
import * as entryActions from '@/actions/entry';

export const router: express.Router = express.Router();

const subRouter: express.Router = express.Router({mergeParams: true});
register(subRouter, __dirname, {exclude: ['dto', 'index', 'utils']});

router.post('/', requireAuth, AsyncREST(addEvent));
router.get('/', AsyncREST(getEvents));
router.get('/:eventIdOrSlug/allData', AsyncREST(getAllEventData));
router.post('/:eventId/allData', requireAuth, AsyncREST(updateAllEventData));
router.get('/:eventId/entries', AsyncREST(getEntries));
router.get('/:eventSlugOrId', AsyncREST(getEvent));
router.put('/:eventId', requireAuth, AsyncREST(updateEvent));
router.delete('/:eventId', requireAuth, AsyncREST(deleteEvent));
router.use('/:eventId', subRouter);

async function getAllEventData(req: express.Request, res: express.Response) {
  let sinceDateStr = queryToString(req.query.sinceDate);
  let sinceDate: Date | undefined;
  if (sinceDateStr) {
    sinceDate = new Date(sinceDateStr);
    if (isNaN(sinceDate.getTime())) { sinceDate = (void 0); }
  }
  return await eventActions.getAllData(req.params.eventIdOrSlug, sinceDate, req.user?._id);
}
async function updateAllEventData(req: express.Request, res: express.Response) {
  res.send(await eventActions.updateAllData(req.body, req.user?._id));
}

async function getEvents(req: express.Request, res: express.Response) {
  if (req.user) {
    return await eventActions.getAll(req.user._id);
  } else {
    // This is on purpose so we don't send any arguments. We don't want to send
    // (void 0) as an argument
    return await eventActions.getAll();
  }
}

async function getEvent(req: express.Request, res: express.Response) {
  const result = await eventActions.find(req.params.eventSlugOrId);
  return result;
}

async function deleteEvent(req: express.Request, res: express.Response) {
  const result = await eventActions.remove(req.params.eventId, req.user?._id);
  return result;
}

async function addEvent(req: express.Request, res: express.Response) {
  return await eventActions.create(req.body, req.user?._id);
}
async function updateEvent(req: express.Request, res: express.Response) {
  return await eventActions.update(req.body.id, req.body, req.user?._id);
}

async function getEntries(req: express.Request, res: express.Response) {
  return await entryActions.getAll({eventId: req.params.eventId});
}
