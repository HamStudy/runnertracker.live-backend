import { express, logger} from '@/api';
import { AsyncREST, queryToString } from '@/lib/express.utils';
import * as Errors from '@/lib/errors';
import { requireAuth } from '@/middleware/authorization';
import * as entryActions from '@/actions/entry';

export const router: express.Router = express.Router();

router.post('/', requireAuth, AsyncREST(addEntry));
router.get('/', AsyncREST(getEntry));
router.get('/:entryId', AsyncREST(getEntry));
router.put('/:entryId', requireAuth, AsyncREST(updateEntry));
router.delete('/:entryId', requireAuth, AsyncREST(deleteEntry));

async function addEntry(req: express.Request, res: express.Response) {
  return await entryActions.create(req.body, req.user?._id);
}

async function getEntry(req: express.Request, res: express.Response) {
  const entryId = req.params.entryId;
  const participantId = queryToString(req.query.participantId);
  const stationId = queryToString(req.query.stationId);
  if (!entryId && (!participantId || !stationId)) {
    return new Errors.InvalidRequestError('Missing participantId or stationId on query params');
  }
  const result = await (entryId ? entryActions.find(entryId) : entryActions.find(participantId, stationId));
  return result;
}

async function updateEntry(req: express.Request, res: express.Response) {
  return await entryActions.update(req.params.entryId, req.body, req.user?._id);
}

async function deleteEntry(req: express.Request, res: express.Response) {
  const result = await entryActions.remove(req.params.entryId, req.user?._id);
  return result;
}
