import * as bodyParser from 'body-parser';
import { express, logger, register } from '..';
import { REQUEST_BODY_LIMIT } from '../../app';

export const router: express.Router = express.Router();

router.use(bodyParser.json({limit: REQUEST_BODY_LIMIT}));
// router.use(setHeaders);

register(router, __dirname, {exclude: ['dto', 'index', 'utils']});

/**
 * Make sure the appropriate headers are sent back
 * @param {Router.IRouterContext} ctx  [description]
 * @param {Function}              next [description]
 */
async function setHeaders(req: express.Request, res: express.Response, next: express.NextFunction) {
    res.setHeader('Content-Type', 'application/json');
    await next();
}
