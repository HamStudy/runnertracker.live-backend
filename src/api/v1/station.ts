import { express, logger} from '@/api';
import * as Errors from '@/lib/errors';
import { AsyncREST, queryToString } from '@/lib/express.utils';
import { requireAuth } from '@/middleware/authorization';
import * as stationActions from '@/actions/stations';
import * as entryActions from '@/actions/entry';

export const router: express.Router = express.Router();

router.post('/', requireAuth, AsyncREST(addStation));
router.get('/:stationId', AsyncREST(getStation));
router.get('/:stationId/entries', AsyncREST(getEntries));
router.put('/:stationId', requireAuth, AsyncREST(updateStation));
router.delete('/:stationId', requireAuth, AsyncREST(deleteStation));

async function addStation(req: express.Request, res: express.Response) {
  return await stationActions.create(req.body, req.user?._id);
}

async function getStation(req: express.Request, res: express.Response) {
  const result = await stationActions.find(req.params.stationId, queryToString(req.query.event) || (void 0))
  return result;
}

async function updateStation(req: express.Request, res: express.Response) {
  return await stationActions.update(req.body.id, req.body, req.user?._id);
}

async function deleteStation(req: express.Request, res: express.Response) {
  let result = await stationActions.remove(req.params.stationId, req.user?._id);
  return result;
}

async function getEntries(req: express.Request, res: express.Response) {
  const sinceDate = req.query.sinceDate ? new Date(req.query.sinceDate as string) : (void 0);
  return await entryActions.getAll({stationId: req.params.stationId, sinceDate});
}
