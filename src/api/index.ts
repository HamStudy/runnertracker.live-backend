import express from 'express';
import { basename, sep, relative, join } from 'path';

import { load, LoadOptions } from '../lib/file.utils';
import logger from '../lib/logger';

/**
 * Export items which are expected to be common among other API
 * files such as the logger instance and Router implementation.
 */
export { logger, express };

export const router: express.Router = express.Router();

/**
 * Make a router with a specific prefix
 * @param {string} path Path from which to generate the router prefix
 * @param {number} numPathParts use the last x components of the path
 *                              when generating the prefix
 */
export function makeRegisteredRouter(path: string, numPathParts: number = 1): express.Router {
    const prefix = path.split(sep).slice(-1 * numPathParts).join('/');
    const rtr = express.Router();
    router.use(`/${prefix}`, router);
    return rtr;
}

/**
 * Register router on the express app
 * @param {express.Application} app  express app to register router on
 */
export function register(rtr: express.Router | express.Router, path: string = '', opts?: LoadOptions) {
    // special case '.' so we can call register from app.ts
    if (!path) {
        path = __dirname;
        // the docs path doesn't expose a router and should be
        // registered specifically.
        opts = {exclude: ['index', 'docs']};
    }
    logger.info(`register called for: ${path}`);
    load(path, registerRouters, opts);

    /**
     * Registers routes from other modules.
     * @param {Router}} mod     module
     * @param {string}   modName module name
     */
    function registerRouters(mod: {router?: express.Router}, modName: string, path: string) {
        logger.info(`loading routes from: ${modName}`);
        if (!(mod && mod.router)) {
            logger.warn(`No router register function exported: ${modName}`);
            return;
        }
        const baseUri = join('/', modName);
        rtr.use(baseUri, mod.router);
    }
}
