import { express, logger} from '..';
import * as Errors from '@/lib/errors';
import { AsyncREST } from '@/lib/express.utils';
import { User } from '../../models/user';
import { requireAdminAuth, requireAuth } from '../../middleware/authorization';
import JWTSessionModel from '@/models/jwtSession';

export const router: express.Router = express.Router();

interface UserLoginBody {
  accessToken?: string;
  username?: string;
  password?: string;
}

router.get('/', requireAuth, AsyncREST(getUser));
router.post('/', requireAdminAuth, AsyncREST(addUser));
router.post('/login', AsyncREST(login));
router.get('/logout', AsyncREST(logout));

async function login(req: express.Request, res: express.Response) {
  const body: UserLoginBody = req.body;
  if (!(body && (body.username && body.password || body.accessToken))) {
    return new Errors.InvalidRequestError('Missing username, password, or accessToken')
  }
  const username = body.username;
  const password = body.password;
  const accessToken = body.accessToken;

  const user = await User.getUser({username, password, accessToken});
  if (!user) {
    return new Errors.UnauthenticatedError('Invalid username or password');
  }

  const jwtSession = new JWTSessionModel({expiresAt: Date.now() + (7 * 24 * 60 * 60 * 1000), userId: user._id});
  await jwtSession.save!();
  const token = await jwtSession.getBearerToken();
  res.setHeader('authentication', `Bearer ${token}`);
  return token;
}

async function logout(req: express.Request, res: express.Response) {
  if (!req.jwtSessionId) { return; }
  await JWTSessionModel.findByIdAndDelete(req.jwtSessionId);
}

async function getUser(req: express.Request, res: express.Response) {
  return req.user ? getReturnableUserProps(req.user) : new Errors.UnauthenticatedError();
}

async function addUser(req: express.Request, res: express.Response, next: express.NextFunction) {
  const body = req.body;
  if (!req.user) { return new Errors.UnauthenticatedError(); }
  if (!(body && body.firstName && body.lastName && body.username)) {
    return new Errors.InvalidRequestError('Missing firstName, lastName, username')
  }
  let user = new User();
  if (body.accessToken) {
    user.accessToken = body.accessToken;
  }
  user.firstName = body.firstName;
  user.lastName = body.lastName;
  user.username = body.username;
  if (body.password) {
    user.setPassword(body.password);
  }
  const u = await user.save!();
  return getReturnableUserProps(u);
}

function getReturnableUserProps({firstName, lastName, username}: User) {
  return { firstName, lastName, username };
}