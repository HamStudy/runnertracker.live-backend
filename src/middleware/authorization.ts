import express from 'express';
import { encode, decode } from 'jwt-simple';
import { ForbiddenError, UnauthenticatedError } from '@/lib/errors';
import logger from '@/lib/logger';
import { User } from '@/models/user';
import { config } from '@/config';
import JWTSessionModel, { TokenData } from '@/models/jwtSession';

export async function requireAuth(req: express.Request, res: express.Response, next: Function) {
  if (req.user) {
    next();
  } else {
    res.sendResponse(new UnauthenticatedError());
  }
}

export async function requireAdminAuth(req: express.Request, res: express.Response, next: Function) {
  if (req.user && req.user.hasRole('superuser')) {
    next();
  } else if (req.user) {
    res.sendResponse(new ForbiddenError());
  } else {
    res.sendResponse(new UnauthenticatedError());
  }
}


export async function parseAuth(req: express.Request, res: express.Response, next: Function) {
  if (!config.api.authRequired) {
    req.user = new User({firstName: 'HamStudy', lastName: 'Company', username: 'runnertracker'});
    return next();
  }
  if (req.headers.authentication) {
    try {
      let authStr = Array.isArray(req.headers.authentication) ? req.headers.authentication[0] : req.headers.authentication;
      if (authStr.substring(0, 7).toLowerCase() !== 'bearer ') { throw new UnauthenticatedError(); }
      const token = authStr.substring(7);
      const tokenData: TokenData = decode(token, config.jwt.secret);
      const jwtSession = await JWTSessionModel.findById(tokenData.jwtSessionId).populate('user').exec();
      const user = jwtSession?.user;
      if (user) {
        req.user = user;
        req.jwtSessionId = jwtSession?._id;
      }
    } catch(e) {
      logger.error(e);
    }
  }
  next();
}

