import './initPathAlias';
import bodyParser from 'body-parser';
import express from 'express';
import cors from 'cors';
import { Server } from 'http';
import { join } from 'path';

import * as api from '@/api';
import Syncronizer from '@/actions/sync';
import { bootstrap } from '@/bootstrap-data';
import { config } from '@/config';
import * as db from '@/db';
import logger from '@/lib/logger';
import { parseAuth } from './middleware/authorization';
import sendresponse from '@/lib/SendResponse';
import { translateMongoError } from './models/plugins/mongoErrorTranslate';


function configureModules() {
    // Configure sendresponse error handlers
    sendresponse.registerTranslator(translateMongoError);
    sendresponse.setVerboseLogs();
    // sendresponse.setLegacyErrorFormat();
}

export const REQUEST_BODY_LIMIT = 1024 * 1024 * 10;  // Allow up to 10 MB request sizes (JSON for file upload, URLENCODED form posts)

let syncronizer: Syncronizer;

const app = express();
app.use(cors());
app.use(sendresponse.middleware);

configureModules();
async function configureServer() {
  logger.debug('Configuring Server...');

  await db.connect().catch((err: any) => {
    logger.error('DB connection error', err);
    process.exit(-1);
  });

  app.use(bodyParser.json({limit: REQUEST_BODY_LIMIT}));
  app.use(bodyParser.urlencoded({ extended: true, limit: REQUEST_BODY_LIMIT }));

  // Configure session and authentication
  logger.trace('Setup session store');

  // APIs
  logger.trace('Setup api routes');
  api.router.use(parseAuth);
  api.register(api.router);
  api.router.use((err: any, req: express.Request, res: express.Response, next: express.NextFunction) => {
    if (err) { res.sendResponse(err); }
    next();
  });
  app.use('/api', api.router);


  // Static / app routes
  logger.trace('Setup static routes for app');
  app.use('/app', express.static(join(__dirname, '..', config.staticFiles.app)));
  app.use('/app', fallback('index.html', join(__dirname + '/..', config.staticFiles.app)));
  app.use(express.static(join(__dirname, '..', config.staticFiles.livePortal)));
  app.use(fallback('index.html', join(__dirname + '/..', config.staticFiles.livePortal), /^\/(api|app)\//));
}

function syncIfEnabled() {
  if (!config.synchronizer.enabled) {
    return;
  }
  logger.info(`setting up sync to ${config.synchronizer.serverAddr}`);
  const syncOpts = config.synchronizer;
  syncronizer = new Syncronizer(
    syncOpts.serverAddr,
    syncOpts.eventId,
    syncOpts.timeBetweenSyncs,
    syncOpts.whoWins,
    {
      accessToken: syncOpts.accessToken,
      skipPush: syncOpts.skipPush,
      localUserId: syncOpts.localUserId,
    }
  );
  syncronizer.runContinuously();
}

let server: Server;
export async function startServer() {
  if (server) { return server; }
  await configureServer();
  await bootstrap();
  syncIfEnabled();
  logger.info('Starting server...');
  server = app.listen({port: config.server.port, host: config.server.host}, () => {
    logger.info(`\nlistening on port ${config.server.host}:${config.server.port}`);
  });
  server.on('error', (err: any) => {
    logger.error("Failed to start server", err);
  });
  registerProcessHandlers();
  return server;
}

/**
 * Fallback to file url does not resolve
 */
function fallback(file: string, dir: string, excludeRoutes?: RegExp) {
  return (req: express.Request, res: express.Response, next: express.NextFunction) => {
    if (excludeRoutes && excludeRoutes.test(req.url)) { next(); }
    if ((req.method === 'GET' || req.method === 'HEAD') && req.accepts('html')) {
      res.sendFile.call(res, file, { root: dir }, (err: any) => err && next())
    } else {
      next();
    }
  }
}

function registerProcessHandlers() {
  process
    .once('SIGINT', () => {
      logger.warn('server shutting due to interrupt');
      server.close(() => {
        db.disconnect();
      });
      syncronizer?.stopSyncing();
    })
    .once('SIGTERM', () => {
      logger.error('server terminated');
      server.close(() => {
        db.disconnect();
      });
      syncronizer?.stopSyncing();
    });
}



if (require.main === module) {
  startServer();
}
