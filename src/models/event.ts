import mongoose from 'mongoose';

import {
  dateField,
  field,
  ModelFromSchemaDef,
  ref,
  required,
  schemaDef,
} from 'mongoose-decorators-ts';
import expiresAtPlugin from './plugins/expires';


/**
 * Defines the schema for race events registered.
 */
@schemaDef({
  indexes: [
    [{slug: 1}, {unique: true, collation: { locale: 'en', strength: 1 }, partialFilterExpression: { slug: { $exists: true, $gt: '' }, expiresAt: null }}],
  ],
  plugins: [
    {plugin: expiresAtPlugin, options: {index: true, sparse: true, default: null}},
  ],
  schemaOpts: {
    timestamps: true,
    toJSON: {virtuals: true},
  }
})
export class Event {
  @required name: string;
  @dateField startDate: Date;
  @dateField endDate: Date;
  @field slug?: string;

  /** Added by setting `timestamps: true` in schema_options */
  createdAt: Date;
  /** Added by setting `timestamps: true` in schema_options */
  updatedAt: Date;

  /** Added by expires plugin */
  expiresAt: Date | null;;

  @ref('Users', {required: false})
  userId!: mongoose.Types.ObjectId;
}

/**
 * Generates a `mongoose.Model` for a given connection.
 * @param {mongoose.Connection} conn [optional] mongoose connection on which to
 *                                   register the model. Default: [mongoose.connection]
 */
export function getModel(conn: mongoose.Connection = mongoose.connection) {
  return ModelFromSchemaDef(Event, conn);
}


export const EventModel = getModel(mongoose.connection);
export type EventModel = mongoose.Document<Event>;
export default EventModel;
