import mongoose from 'mongoose';

import {
  dateField,
  field,
  ModelFromSchemaDef,
  ref,
  schemaDef,
} from 'mongoose-decorators-ts';
import expiresAtPlugin from './plugins/expires';


/**
 * Defines the schema for race events registered.
 */
@schemaDef({
  indexes: [
    [{stationId: 1, participantId: 1}, {unique: true, partialFilterExpression: { expiresAt: null }}],
    [{eventId: 1}],
    [{participantId: 1}],
  ],
  plugins: [
    {plugin: expiresAtPlugin, options: {index: true, sparse: true, default: null}},
  ],
  schemaOpts: {
    timestamps: true,
    toJSON: {virtuals: true},
  }
})
export class Entry {
  @ref('Events', {required: true})
  eventId: mongoose.Types.ObjectId;
  @ref('Participants', {required: true})
  participantId!: mongoose.Types.ObjectId;
  @ref('Stations', {required: true})
  stationId!: mongoose.Types.ObjectId;

  @dateField timeIn: Date;
  @dateField timeOut: Date;

  /** Added by setting `timestamps: true` in schema_options */
  createdAt: Date;
  /** Added by setting `timestamps: true` in schema_options */
  updatedAt: Date;

  /** Added by expires plugin */
  expiresAt: Date | null;

  @ref('Users', {required: false})
  userId!: mongoose.Types.ObjectId;
}

/**
 * Generates a `mongoose.Model` for a given connection.
 * @param {mongoose.Connection} conn [optional] mongoose connection on which to
 *                                   register the model. Default: [mongoose.connection]
 */
export function getModel(conn: mongoose.Connection = mongoose.connection) {
  return ModelFromSchemaDef(Entry, conn);
}


export const EntryModel = getModel(mongoose.connection);
export type EntryModel = mongoose.Document<Entry>;
export default EntryModel;
