import mongoose from 'mongoose';

import {
  arrayField,
  field,
  ModelFromSchemaDef,
  ref,
  required,
  schemaDef,
} from 'mongoose-decorators-ts';
import expiresAtPlugin from './plugins/expires';


@schemaDef({
  schemaOpts: {
    _id: false,
    id: false,
    toJSON: {virtuals: true},
  }
})
class Station {
  @ref('Station', {required: true})
  id: mongoose.Types.ObjectId;
  @field name?: string;
  @field stationNumberDisplayed?: string;
  @field distance?: number;
}
type StationSubdoc = mongoose.Types.OptionalSubdocument<Station>;


/**
 * Defines the schema for race events registered.
 */
@schemaDef({
  indexes: [
    [{eventId: 1, name: 1}, {unique: true, partialFilterExpression: { expiresAt: null }}],
  ],
  plugins: [
    {plugin: expiresAtPlugin, options: {index: true, sparse: true, default: null}},
  ],
  schemaOpts: {
    timestamps: true,
    toJSON: {virtuals: true},
  }
})
export class Race {
  @ref('Events', { required: true })
  eventId: mongoose.Types.ObjectId;
  @required name: string;

  @arrayField(Station, {required: true})
  stations: mongoose.Types.DocumentArray<StationSubdoc>;

  /** Added by setting `timestamps: true` in schema_options */
  createdAt: Date;
  /** Added by setting `timestamps: true` in schema_options */
  updatedAt: Date;

  /** Added by expires plugin */
  expiresAt: Date | null;;

  @ref('Users', {required: false})
  userId!: mongoose.Types.ObjectId;
}

/**
 * Generates a `mongoose.Model` for a given connection.
 * @param {mongoose.Connection} conn [optional] mongoose connection on which to
 *                                   register the model. Default: [mongoose.connection]
 */
export function getModel(conn: mongoose.Connection = mongoose.connection) {
  return ModelFromSchemaDef(Race, conn);
}


export const RaceModel = getModel(mongoose.connection);
export type RaceModel = mongoose.Document<Race>;
export default RaceModel;
