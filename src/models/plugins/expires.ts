import mongoose from 'mongoose';

export type DefaultFn = () => Date;

export interface PluginOpts {
  /** default value to set on newly created documents */
  default?: Date | DefaultFn;
  /** how long after timestamp reached should the document be deleted? (default: 0) */
  expireAfterSeconds?: number;
  /** create the TTL index? */
  index?: boolean;
  /** path for the expires at field (default: expiresAt) */
  path?: string;
  /** indicates the index should be sparse */
  sparse?: boolean;
}

export function expiresAtPlugin(schema: mongoose.Schema, opts: PluginOpts = {}) {
  const expiresAt = opts.path || 'expiresAt';
  const expiresAtOpts: any = {type: Date};
  const partialFilterExpression = {expiresAt: {$type: 'date', $exists: true}};
  if (opts.default) { expiresAtOpts.default = opts.default; }
  schema.add({
    [expiresAt]: expiresAtOpts,
  });
  if (opts.index || opts.sparse) {
    const idxOpts = {
      expireAfterSeconds: opts.expireAfterSeconds || 0,
      ...(opts.sparse ? {partialFilterExpression} : {}),
    };
    schema.index({[expiresAt]: 1}, idxOpts);
  }
}

export default expiresAtPlugin;
