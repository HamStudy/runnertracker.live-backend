function parseErrorPaths(err: MongoError) {
    return Object.keys(err.errors || {});
}

export interface MongoError extends Error {
    code: number;
    err: string;
    errors?: {[key: string]: ValidationError};
    kind?: string;
    path?: string;
    value?: any;
}

interface ValidationError extends Error {
    kind: string;
    path: string;
    value?: any;
}

export function translateMongoError(err: MongoError) {
    let errorPaths: string[] = [],
        ret: any   = null,
        results: any = null;

    switch(err.name) {
        case 'MongoError':
            if(err.code === 11000 || err.code === 11001) { // 11000 is new item, 11001 is existing item
                let data: any = {};
                data.collection = err.message.split(' ').find(x => x.indexOf('.') !== -1)?.split('.')[1];
                data.index = err.message.split('index: ')[1].split(' ')[0];
                results = {
                    type:'DuplicateKey',
                    data: [data],
                    code: 409
                };
                ret     = {result: results, code: 409};
            } else {
                results = {type:'MongoError', data: ['Unknown'], code: 500};
                ret     = {result: results, code: 500};
            }
            break;
        case 'ValidationError':
            errorPaths = parseErrorPaths(err);
            if (errorPaths.length) {
                results = {type:'ValidationError', data: errorPaths, code: 400};
                ret     = {result: results, code: 400};
            } else {
                results = {type:'ValidationError', data: ['Unknown'], code: 500};
                ret     = {result: results, code: 500};
            }
            break;
        case 'CastError':
            results = {
                type:'CastError',
                data: {kind: err.kind, value: err.value, path: err.path},
                code: 500
            };
            ret     = {result: results, code: 500};
            break;
        default:
            return null;
    }
    return ret;
}
