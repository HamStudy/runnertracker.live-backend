import mongoose, { Types, VirtualType } from 'mongoose';
import { encode, decode } from 'jwt-simple';
import {
  ModelFromSchemaDef,
  populateVirtual,
  ref,
  schemaDef,
} from 'mongoose-decorators-ts';
import expiresAtPlugin from './plugins/expires';
import { config } from '@/config';
import { User } from './user';

export interface TokenData {
  jwtSessionId: string;
  exp: number;
  iat: number;
}

/**
 * Defines the schema for race events registered.
 */
@schemaDef({
  indexes: [
  ],
  plugins: [
    {plugin: expiresAtPlugin, options: {index: true, sparse: true, default: null}},
  ],
  schemaOpts: {
    timestamps: true,
    toJSON: {virtuals: true},
  }
})
export class JWTSession {
  _id: Types.ObjectId;
  /** Added by setting `timestamps: true` in schema_options */
  createdAt: Date;
  /** Added by setting `timestamps: true` in schema_options */
  updatedAt: Date;

  /** Added by expires plugin */
  expiresAt: Date;

  @ref('Users', {required: true})
  userId!: mongoose.Types.ObjectId;

  @populateVirtual({
    ref: 'User',
    foreignField: '_id',
    localField: 'userId',
    justOne: true,
  })
  user?: User;

  static async getBearerToken(jwtSession: mongoose.LeanDocument<JWTSessionModel>) {
    if (!jwtSession?._id?.toHexString()) { return null; }
    const data: TokenData = {
      exp: jwtSession.expiresAt?.getTime(),
      iat: jwtSession.createdAt.getTime(),
      jwtSessionId: jwtSession._id.toHexString(),
    };
    return encode(data, config.jwt.secret);
  }
  getBearerToken(this: mongoose.LeanDocument<JWTSessionModel>) {
    return JWTSession.getBearerToken(this);
  }
}

/**
 * Generates a `mongoose.Model` for a given connection.
 * @param {mongoose.Connection} conn [optional] mongoose connection on which to
 *                                   register the model. Default: [mongoose.connection]
 */
export function getModel(conn: mongoose.Connection = mongoose.connection) {
  return ModelFromSchemaDef(JWTSession, conn);
}


export const JWTSessionModel = getModel(mongoose.connection);
export type JWTSessionModel = mongoose.Document<JWTSession>;
export default JWTSessionModel;
