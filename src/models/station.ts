import mongoose from 'mongoose';

import {
  field,
  ModelFromSchemaDef,
  ref,
  required,
  schemaDef,
} from 'mongoose-decorators-ts';
import expiresAtPlugin from './plugins/expires';


/**
 * Defines the schema for race events registered.
 */
@schemaDef({
  indexes: [
    [{eventId: 1, stationNumber: 1}, {unique: true, partialFilterExpression: { expiresAt: null }}],
  ],
  plugins: [
    {plugin: expiresAtPlugin, options: {index: true, sparse: true, default: null}},
  ],
  schemaOpts: {
    timestamps: true,
    toJSON: {virtuals: true},
  }
})
export class Station {
  @ref('Events', { required: true })
  eventId: mongoose.Types.ObjectId;
  @field name?: string;
  @required stationNumber!: number;
  @field stationNumberDisplayed!: string;
  @field distance?: number;
  @ref('Races', {required: false})
  raceId: mongoose.Types.ObjectId;

  /** Added by setting `timestamps: true` in schema_options */
  createdAt: Date;
  /** Added by setting `timestamps: true` in schema_options */
  updatedAt: Date;

  /** Added by expires plugin */
  expiresAt: Date | null;;

  @ref('Users', {required: false})
  userId!: mongoose.Types.ObjectId;
}

/**
 * Generates a `mongoose.Model` for a given connection.
 * @param {mongoose.Connection} conn [optional] mongoose connection on which to
 *                                   register the model. Default: [mongoose.connection]
 */
export function getModel(conn: mongoose.Connection = mongoose.connection) {
  return ModelFromSchemaDef(Station, conn);
}


export const StationModel = getModel(mongoose.connection);
export type StationModel = mongoose.Document<Station>;
export default StationModel;
