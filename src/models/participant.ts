import mongoose, { Types } from 'mongoose';

import {
  field,
  ModelFromSchemaDef,
  ref,
  required,
  schemaDef,
} from 'mongoose-decorators-ts';
import expiresAtPlugin from './plugins/expires';


/**
 * Defines the schema for race events registered.
 */
@schemaDef({
  indexes: [
    [{bibNumber: 1, eventId: 1}, {unique: true, partialFilterExpression: { expiresAt: null }}],
    [{bibNumber: 1, eventId: 1, raceId: 1}, {unique: true, partialFilterExpression: { expiresAt: null }}],
  ],
  plugins: [
    {plugin: expiresAtPlugin, options: {index: true, sparse: true, default: null}},
  ],
  schemaOpts: {
    timestamps: true,
    toJSON: {virtuals: true},
  }
})
export class Participant {
  _id!: Types.ObjectId;
  id!: string;
  @field(String) age?: string;
  @required bibNumber: number;
  @ref('Events', {required: true})
  eventId: mongoose.Types.ObjectId;
  @field firstName?: string;
  @field lastName?: string;
  @field note?: string;
  @field home?: string;
  @ref('Races', {required: false})
  raceId?: mongoose.Types.ObjectId;
  @field sex?: 'M' | 'F';
  @field team?: string;
  @field dnfReason: string;
  @field(Number) dnfStation?: number;

  /** Added by setting `timestamps: true` in schema_options */
  createdAt: Date;
  /** Added by setting `timestamps: true` in schema_options */
  updatedAt: Date;

  /** Added by expires plugin */
  expiresAt: Date | null;;

  @ref('Users', {required: false})
  userId!: mongoose.Types.ObjectId;
}

/**
 * Generates a `mongoose.Model` for a given connection.
 * @param {mongoose.Connection} conn [optional] mongoose connection on which to
 *                                   register the model. Default: [mongoose.connection]
 */
export function getModel(conn: mongoose.Connection = mongoose.connection) {
  return ModelFromSchemaDef(Participant, conn);
}


export const ParticipantModel = getModel(mongoose.connection);
export type ParticipantModel = mongoose.Document<Participant>;
export default ParticipantModel;
