import * as crypto from 'crypto';
import { encode, decode } from 'jwt-simple';
import * as mongoose from 'mongoose';
import {
  arrayField,
  field,
  IMongooseDocument, ModelFromSchemaDef,
  lower, ref, required,
  schemaDef,
} from 'mongoose-decorators-ts';
import { config } from '@/config';
import JWTSessionModel, { TokenData } from './jwtSession';


export type Role = 'superuser';

/**
 * Defines the schema for users registered on tech team test tools.
 */
@schemaDef({
  indexes: [
    [{username: 1}, {unique: true}],
  ],
  schema_options: {id: false},
})
export class UserSchema {
  _id: mongoose.Types.ObjectId;

  @required firstName: string;
  @required lastName: string;
  @required @lower username: string;
  @required password: string;
  @field accessToken: string;
  @arrayField(String) roles: Role[];

  setPassword(password: string) {
    this.password = hashPassword(password);
  }

  static async getUser({accessToken, username, password, token}: {accessToken?: string; username?: string; password?: string; token?: string}) {
    if (token) {
      const tokenData: TokenData = decode(token, config.jwt.secret);
      const jwtSession = await JWTSessionModel.findById(tokenData.jwtSessionId).populate('user').exec();
      return jwtSession?.user || null;
    } else if (username && password) {
      password = hashPassword(password);
      const user = await User.findOne({username, password}).exec();
      return user;
    } else if (accessToken) {
      const user = await User.findOne({accessToken}).exec();
      return user;
    } else {
      return null;
    }
  }

  // static async getBearerToken(user: mongoose.LeanDocument<User>) {
  //   const data: TokenData = {

  //   };
  //   return encode({user: user.username}, config.jwt.secret);
  // }
  // getBearerToken(this: User) {
  //   return UserSchema.getBearerToken(this);
  // }

  static hasRole(user: mongoose.LeanDocument<User>, role: Role) {
    return user.roles.indexOf(role) > -1;
  }
  hasRole(this: User, role: Role) {
    return UserSchema.hasRole(this, role);
  }
}

export const User = getModel();
export type User = IMongooseDocument<UserSchema>;

/**
 * Generates a `mongoose.Model` for a given connection.
 * @param {mongoose.Connection} conn [optional] mongoose connection on which to
 *                                   register the model. Default: [mongoose.connection]
 */
export function getModel(conn: mongoose.Connection = mongoose.connection) {
  return ModelFromSchemaDef<typeof UserSchema, UserSchema>(UserSchema, conn);
}

export function hashPassword(password:string) {
  return crypto.createHash("md5").update(password).digest("hex");
}
