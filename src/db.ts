import {readFileSync} from 'fs';
import mongoose from 'mongoose';
import { config, SSLOptions } from './config';
// import {recordStartupTime} from './lib/nsolid.utils';
// import {initDbMonitoring} from './lib/monitoring.utils';
import { getLogger } from './lib/logger';

const logger = getLogger('db');

// re-export for convenience
export { config, logger };

if (config.env === 'cli') { registerHandlers(); }

/**
 * Close the mongoose connection.
 */
export async function disconnect() {
    logger.trace('disconnecting mongoose');
    return mongoose.disconnect();
}

/**
 * Helper method used to load SSL secrets from the filesystem.
 */
function getSSLOptions() {
    let sslOpts: SSLOptions = Object.assign({}, <any>config.db.ssl);
    Object.keys(sslOpts).forEach(key => {
        const filename = sslOpts[key];
        if (typeof filename !== 'string') { return; }
        if (sslOpts[key]) {
            try {
                sslOpts[key] = readFileSync(filename).toString();
                sslOpts.ssl = true;
            } catch(err) {
                logger.error(`unable to read file: ${filename}`);
            }
        }
    });
    return sslOpts;
}


/**
 * Connect to MongoDB
 * @param {string} uri  DB URI
 * @param {any}    opts DB connection options
 */
export async function connect(uri: string = config.db.uri, opts?: any) {
    logger.debug({uri, opts});
    opts = Object.assign({}, config.db.opts, opts, getSSLOptions(), {promiseLibrary: global.Promise});
    logger.trace('calling mongoose.connect');
    return mongoose.connect(uri, opts);
}

/**
 * Close the DB connection upon application termination.
 */
export async function gracefulExit(): Promise<void> {
    await disconnect().then(() => {
        logger.info(`mongoose connection closed due to app termination.`);
    });
    return;
}

/**
 * Setup handlers for connection events.
 */
mongoose.connection.on('connected', (event: any) => {
    // recordStartupTime('mongoose_connect');
    logger.info(`mongoose connected.`);
    // initDbMonitoring(mongoose.connection.db, 'mongoose', true);
});

mongoose.connection.on('disconnected', () =>
    logger.info(`mongoose disconnected.`)
);

mongoose.connection.on('err', (err: Error) =>
    logger.error(err)
);

/**
 * Register handlers for process cleanup.
 */
export function registerHandlers() {
    process
        .on('exit', disconnect)
        .once('SIGINT', gracefulExit)
        .once('SIGTERM', gracefulExit);
}

/**
 * Used by cli-tools to connect to the database and make sure
 * proper exit handlers are installed.
 */
export async function main() {
    registerHandlers();
    return connect();
}
