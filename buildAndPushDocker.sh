VERSION=$(git describe)
docker build -t registry.gitlab.com/hamstudy/runnertracker.live-backend:$VERSION . \
&& docker tag registry.gitlab.com/hamstudy/runnertracker.live-backend:$VERSION registry.gitlab.com/hamstudy/runnertracker.live-backend:latest \
&& docker push registry.gitlab.com/hamstudy/runnertracker.live-backend:$VERSION && docker push registry.gitlab.com/hamstudy/runnertracker.live-backend:latest