# ---- Base Node Image ----
FROM node:16-alpine AS base

RUN apk update && apk upgrade && \
    apk add --no-cache git python3 make g++
RUN ln -sf python3 /usr/bin/python

RUN mkdir /app && chown node:node /app
USER node
WORKDIR /app

COPY --chown=node:node package.json .

# ---- Build ----
FROM base AS build
ENV NODE_ENV=development

COPY --chown=node:node . .
RUN echo "{\"VERSION\":\"$(git describe)\"}" > BUILDINFO.json
RUN cat BUILDINFO.json
RUN npm install
RUN npm run build

# ---- production node_modules
FROM build AS npmprod
ENV NODE_ENV=production
RUN rm -rf node_modules
RUN npm install


#
# ---- Test ----
# run linters, setup and tests
# FROM build AS test
# COPY . .
# RUN npm run test


# ---- Release ----
FROM base AS release
ENV NODE_ENV=production

# copy production node_modules
COPY --from=npmprod /app/node_modules ./node_modules

# copy app
COPY --chown=node:node --from=build /app/dist ./dist
COPY --chown=node:node --from=build /app/dist-livePortal ./dist-livePortal
COPY --chown=node:node --from=build /app/dist-app ./dist-app
COPY --chown=node:node --from=build /app/BUILDINFO.json .

# expose port and define CMD
EXPOSE 9002
CMD ["npm", "run", "dist"]
